<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('/categoria', 'CategoriaController')->parameters([
    'categoria' => 'categoria',
]);
