<?php

namespace App\Models\Produto;

use App\Models\Produto\Produto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estoque extends Model
{
    use HasFactory;

    protected $table      = 'estoques';
    protected $primaryKey = 'id';
    protected $fillable   = [
        'quantidade',
        'produto_id'
    ];

    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produto_id', 'id');
    }
}
