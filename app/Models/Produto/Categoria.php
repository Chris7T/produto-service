<?php

namespace App\Models\Produto;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;

    protected $table      = 'categorias';
    protected $primaryKey = 'id';
    protected $fillable   = [
        'nome',
    ];

    public function produto()
    {
        return $this->hasOne(Produto::class, 'categoria_id', 'id');
    }
}
