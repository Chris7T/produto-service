<?php

namespace App\Models\Produto;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    use HasFactory;

    protected $table      = 'produtos';
    protected $primaryKey = 'id';
    protected $fillable   = [
        'nome',
        'descricao',
        'preco_atual',
        'categoria_id'
    ];

    protected static function booted()
    {
        static::created(function ($produto) {
            $existeEstoque = Estoque::where('produto_id', $produto->getKey())->exists();
            if (!$existeEstoque) {
                Estoque::create([
                    'produto_id' => $produto->getKey(),
                    'quantidade' => 0
                ]);
            }
        });

        static::deleting(function ($produto) {
            $estoque = Estoque::where('produto_id', $produto->id)->firstOrFail();
            abort_if(($estoque->quantidade > 0), 409, 'Erro, este produto possui estoque.');
            $estoque->delete();
        });
    }

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'categoria_id', 'id');
    }

    public function estoque()
    {
        return $this->hasOne(Estoque::class, 'produto_id', 'id');
    }
}
