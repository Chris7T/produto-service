<?php

namespace App\Http\Requests\Estoque;

use Illuminate\Foundation\Http\FormRequest;

class EstoqueRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'quantidade' => ['required', 'integer', 'gte:0'],
            'produto_id' => ['required', 'integer', 'exists:produtos,id']
        ];
    }

    public function bodyParameters()
    {
        return [
            'produto_id' => [
                'description' => 'ID do Produto.',
                'example'     => '1'
            ],
            'quantidade' => [
                'description' => 'Quantidade do Produto.',
                'example'     => '99'
            ],
        ];
    }
}
