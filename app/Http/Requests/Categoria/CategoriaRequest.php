<?php

namespace App\Http\Requests\Categoria;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => ['required', 'string', 'max:50'],
        ];
    }

    public function bodyParameters()
    {
        return [
            'nome' => [
                'description' => 'Nome da Categoria.',
                'example'     => 'Periféricos.'
            ]
        ];
    }
}
