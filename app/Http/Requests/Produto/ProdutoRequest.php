<?php

namespace App\Http\Requests\Produto;

use Illuminate\Foundation\Http\FormRequest;

class ProdutoRequest extends FormRequest
{
    public function bodyParameters(): array
    {
        return [
            'nome'        => [
                'description' => 'Nome do Produto.',
                'example'     => 'Mouse'
            ],
            'descricao'    => [
                'description' => 'Descrição do Produto.',
                'example'     => 'Garantia de 2 Anos'
            ],
            'preco_atual'  => [
                'description' => 'Preço do Produto.',
                'example'     => '199,90'
            ],
            'categoria_id' => [
                'description' => 'ID da Categoria do Produto.',
                'example'     => '57'
            ]
        ];
    }
}
