<?php

namespace App\Http\Requests\Produto;

class AtualizarProdutoRequest extends ProdutoRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'         => ['filled', 'string', 'max:50'],
            'descricao'    => ['filled', 'string', 'max:100'],
            'preco_atual'  => ['filled', 'numeric', 'gte:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'categoria_id' => ['filled', 'integer', 'exists:categorias,id']
        ];
    }
}
