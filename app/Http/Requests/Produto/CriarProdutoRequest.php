<?php

namespace App\Http\Requests\Produto;

class CriarProdutoRequest extends ProdutoRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'         => ['required', 'string', 'max:50'],
            'descricao'    => ['required', 'string', 'max:100'],
            'preco_atual'  => ['required', 'numeric', 'gte:0', 'regex:/^\d+(\.\d{1,2})?$/'],
            'categoria_id' => ['required', 'integer', 'exists:categorias,id']
        ];
    }
}
