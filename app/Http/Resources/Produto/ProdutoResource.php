<?php

namespace App\Http\Resources\Produto;

use Illuminate\Http\Resources\Json\JsonResource;

class ProdutoResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'            => $this->getKey(),
            'nome'          => $this->nome,
            'descricao'     => $this->descricao,
            'preco_atual'   => $this->preco_atual,
            'categoria'     => $this->categoria->nome,
            'estoque_atual' => $this->estoque->quantidade,
        ];
    }
}
