<?php

namespace App\Http\Controllers\Produto;

use App\Http\Controllers\Controller;
use App\Http\Requests\Estoque\EstoqueRequest;
use RegrasNegocio\Estoque as RegrasEstoque;

class EstoqueController extends Controller
{
    /**
     * Atualizar Estoque
     *
     * Atualizar Estoque
     * @group Estoque
     * @responseFile 200 ApiRespostas/EstoqueController/Buscar.json
     * @responseFile 422 ApiRespostas/EstoqueController/Validacao.json
     */
    public function atualizar(EstoqueRequest $request, RegrasEstoque $regrasEstoque)
    {
        return $regrasEstoque->atualizarEstoque($request);
    }
}
