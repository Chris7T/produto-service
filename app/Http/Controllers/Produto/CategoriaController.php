<?php

namespace App\Http\Controllers\Produto;

use App\Http\Controllers\Controller;
use App\Http\Requests\Categoria\CategoriaRequest;
use App\Http\Resources\Produto\CategoriaResource as Resource;
use App\Models\Produto\Categoria;

class CategoriaController extends Controller
{
    /**
     * Listar dados de Categorias
     *
     * Retorna todos os registros
     * @group Categorias
     * @responseFile ApiRespostas/CategoriaController/Listar.json
     */
    public function index()
    {
        return Resource::collection((Categoria::all()));
    }

    /**
     * Criar nova Categoria
     *
     * Cria nova Categoria
     * @group Categorias
     * @responseFile 201 ApiRespostas/CategoriaController/Buscar.json
     * @responseFile 422 ApiRespostas/CategoriaController/Validacao.json
     */
    public function store(CategoriaRequest $request)
    {
        $novo = Categoria::create($request->all());
        return (new Resource($novo))->response()->setStatusCode(201);
    }

    /**
     * Buscar Categoria
     *
     * Retorna os dados da Categoria
     * @group Categorias
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/CategoriaController/Buscar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Categoria]"}
     */
    public function show(Categoria $categoria)
    {
        return new Resource($categoria);
    }

    /**
     * Atualizar Categoria
     *
     * Atualizar dados Categoria
     * @group Categorias
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/CategoriaController/Buscar.json
     * @responseFile 422 ApiRespostas/CategoriaController/Validacao.json
     * @response 404 {"message": "No query results for model [App\\Models\\Categoria]"}
     */
    public function update(CategoriaRequest $request, Categoria $categoria)
    {
        $categoria->update($request->all());
        return new Resource($categoria);
    }

    /**
     * Excluir Categoria
     *
     * Exclui a Categoria
     * @group Categorias
     * @urlParam id integer required O id do registro.
     * @response 200 {"message": "OK"}
     * @response 409 {"message": "Houve um erro ao tentar realizar está exclusão."}
     * @response 404 {"message": "No query results for model [App\\Models\\Categoria]"}
     */
    public function destroy(Categoria $categoria)
    {
        $categoria->delete();
        return response()->json(['message' => 'Ok']);
    }
}
