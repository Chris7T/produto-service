<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    public const HOME = '/home';

    protected $namespaceProduto = 'App\\Http\\Controllers\\Produto';

    public function boot()
    {
        $this->configureRateLimiting();

        $this->carregarArquivosRotas();
    }

    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }

    private function carregarArquivosRotas()
    {
        $this->routes(
            function () {
                Route::namespace($this->namespace)->middleware('web')->group(base_path('routes/web.php'));
                Route::namespace($this->namespaceProduto)->middleware('api')
                    ->group(base_path('routes/produto.php'));
                Route::namespace($this->namespaceProduto)->middleware('api')
                    ->group(base_path('routes/categoria.php'));
                Route::namespace($this->namespaceProduto)->middleware('api')
                    ->group(base_path('routes/estoque.php'));
            }
        );
    }
}
