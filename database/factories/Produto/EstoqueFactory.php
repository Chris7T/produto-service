<?php

namespace Database\Factories\Produto;

use App\Models\Produto\Estoque;
use App\Models\Produto\Produto;
use Illuminate\Database\Eloquent\Factories\Factory;

class EstoqueFactory extends Factory
{
    protected $model = Estoque::class;

    public function definition()
    {
        return [
            'quantidade' => $this->faker->randomNumber(),
            'produto_id' => Produto::factory()->create()->getKey()
        ];
    }
}
