<?php

namespace Database\Factories\Produto;

use App\Models\Produto\Categoria;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoriaFactory extends Factory
{
    protected $model = Categoria::class;

    public function definition()
    {
        return [
            'nome' => $this->faker->name(),
        ];
    }
}
