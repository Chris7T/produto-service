<?php

namespace Database\Factories\Produto;

use App\Models\Produto\Categoria;
use App\Models\Produto\Produto;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProdutoFactory extends Factory
{
    protected $model = Produto::class;

    public function definition()
    {
        return [
            'nome' => $this->faker->name(),
            'descricao' => $this->faker->realText(100),
            'preco_atual' => $this->faker->randomNumber(),
            'categoria_id' => Categoria::factory()->create()->getKey()
        ];
    }
}
