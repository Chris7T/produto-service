<?php

namespace Tests\Feature\Categoria;

use App\Models\Produto\Categoria;
use Tests\TestCase;

class AtualizarTest extends TestCase
{
    private const ROTA = 'categoria.update';
    private const ID_INVALIDO = 0;
    private int $categoriaId;

    public function setUp(): void
    {
        parent::setUp();
        $this->categoriaId = Categoria::factory()->create()->getKey();
    }

    public function testCategoriaInvalida()
    {
        $valoresGrandes = str_pad('', 101, 'A');
        $categoriaDados   = Categoria::factory()->make([
            'nome'      => $valoresGrandes,
        ]);

        $response = $this->putJson(route(self::ROTA, self::ID_INVALIDO), $categoriaDados->toArray());
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $valoresGrandes = str_pad('', 101, 'A');
        $categoriaDados   = Categoria::factory()->make([
            'nome'      => $valoresGrandes,
        ]);

        $response = $this->putJson(route(self::ROTA, $this->categoriaId), $categoriaDados->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $categoriaDados = [
            'nome' => null,
        ];

        $response = $this->putJson(route(self::ROTA, $this->categoriaId), $categoriaDados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $categoria = [
            'nome'         => 12,
        ];

        $response = $this->putJson(route(self::ROTA, $this->categoriaId), $categoria);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testSucesso()
    {
        $categoria = Categoria::factory()->make();

        $response = $this->putJson(route(self::ROTA, $this->categoriaId), $categoria->toArray());
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'nome',
                ],
            ]);
    }
}
