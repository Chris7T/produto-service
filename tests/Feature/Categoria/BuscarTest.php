<?php

namespace Tests\Feature\Categoria;

use App\Models\Produto\Categoria;
use Tests\TestCase;

class BuscarTest extends TestCase
{
    private const ROTA = 'categoria.show';
    private const ID_INVALIDO = 0;

    public function setUp(): void
    {
        parent::setUp();
        $this->categoriaId = Categoria::factory()->create()->getKey();
    }

    public function testeFalhaProdutoNaoEncontrado()
    {
        $response = $this->getJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $response = $this->getJson(route(self::ROTA, $this->categoriaId));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'nome',
                ],
            ]);
    }
}
