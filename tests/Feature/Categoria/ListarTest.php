<?php

namespace Tests\Feature\Categoria;

use App\Models\Produto\Categoria;
use Tests\TestCase;

class ListarTest extends TestCase
{
    private const ROTA = 'categoria.index';

    public function testSucesso()
    {
        Categoria::factory()->count(3)->create();
        $response = $this->getJson(route(self::ROTA));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'nome',
                    ]
                ],
            ]);
    }
}
