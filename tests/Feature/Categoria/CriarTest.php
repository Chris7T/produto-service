<?php

namespace Tests\Feature\Categoria;

use App\Models\Produto\Categoria;
use Tests\TestCase;

class CriarTest extends TestCase
{
    private const ROTA = 'categoria.store';

    public function testFalhaValoresGrandes()
    {
        $valoresGrandes = str_pad('', 101, 'A');
        $categoriaDados   = Categoria::factory()->make([
            'nome' => $valoresGrandes,
        ]);

        $response = $this->postJson(route(self::ROTA), $categoriaDados->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $categoriaDados = [
            'nome' => null,
        ];

        $response = $this->postJson(route(self::ROTA), $categoriaDados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $categoria = [
            'nome' => 12,
        ];

        $response = $this->postJson(route(self::ROTA), $categoria);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testSucesso()
    {
        $categoria = Categoria::factory()->make();

        $response = $this->postJson(route(self::ROTA), $categoria->toArray());
        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'nome',
                ],
            ]);
    }
}
