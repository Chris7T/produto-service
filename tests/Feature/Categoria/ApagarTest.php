<?php

namespace Tests\Feature\Categoria;

use App\Models\Produto\Categoria;
use Tests\TestCase;

class ApagarTest extends TestCase
{
    private const ROTA = 'categoria.destroy';
    private const ID_INVALIDO = 0;

    public function setUp(): void
    {
        parent::setUp();
        $this->categoriaId = Categoria::factory()->create()->getKey();
    }

    public function testeFalhaProdutoNaoEncontrado()
    {
        $response = $this->deleteJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $response = $this->deleteJson(route(self::ROTA, $this->categoriaId));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);
    }
}
