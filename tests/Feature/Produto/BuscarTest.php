<?php

namespace Tests\Feature\Produto;

use App\Models\Produto\Produto;
use Tests\TestCase;

class BuscarTest extends TestCase
{
    private const ROTA = 'produto.show';
    private const ID_INVALIDO = 0;

    public function setUp(): void
    {
        parent::setUp();
        $this->produtoId = Produto::factory()->create()->getKey();
    }

    public function testeFalhaProdutoNaoEncontrado()
    {
        $response = $this->getJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $response = $this->getJson(route(self::ROTA, $this->produtoId));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria',
                ],
            ]);
    }
}
