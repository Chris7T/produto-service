<?php

namespace Tests\Feature\Produto;

use App\Models\Produto\Produto;
use Tests\TestCase;

class ApagarTest extends TestCase
{
    private const ROTA = 'produto.destroy';
    private const ID_INVALIDO = 0;

    public function setUp(): void
    {
        parent::setUp();
        $this->produtoId = Produto::factory()->create()->getKey();
    }

    public function testeFalhaProdutoNaoEncontrado()
    {
        $response = $this->deleteJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $response = $this->deleteJson(route(self::ROTA, $this->produtoId));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);
    }
}
