<?php

namespace Tests\Feature\Produto;

use App\Models\Produto\Produto;
use Tests\TestCase;

class ListarTest extends TestCase
{
    private const ROTA = 'produto.index';

    public function testSucesso()
    {
        Produto::factory()->count(3)->create();
        $response = $this->getJson(route(self::ROTA));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'descricao',
                        'nome',
                        'preco_atual',
                        'categoria',
                    ]
                ],
            ]);
    }
}
