<?php

namespace Tests\Feature\Produto;

use App\Models\Produto\Estoque;
use App\Models\Produto\Produto;
use Tests\TestCase;

class AtualizarTest extends TestCase
{
    private const ROTA = 'produto.update';
    private const ID_INVALIDO = 0;
    private int $produtoId;

    public function setUp(): void
    {
        parent::setUp();
        $this->produtoId = Produto::factory()->create()->getKey();
        Estoque::factory()->create(['produto_id' => $this->produtoId])->getKey();
    }

    public function testProdutoInvalida()
    {
        $valoresGrandes = str_pad('', 101, 'A');
        $produtoDados   = Produto::factory()->make([
            'nome'      => $valoresGrandes,
        ]);

        $response = $this->putJson(route(self::ROTA, self::ID_INVALIDO), $produtoDados->toArray());
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaCategoriaInvalida()
    {
        $produto = Produto::factory()->make([
            'categoria_id' => self::ID_INVALIDO,
        ]);

        $response = $this->putJson(route(self::ROTA, $this->produtoId), $produto->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'categoria_id',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $produtoDados = [
            'descricao' => null,
            'nome' => null,
            'preco_atual' => null,
            'categoria_id' => null,
        ];

        $response = $this->putJson(route(self::ROTA, $this->produtoId), $produtoDados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria_id',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $produto = [
            'descricao'    => 12,
            'nome'         => 12,
            'preco_atual'  => false,
            'categoria_id' => false,
        ];

        $response = $this->putJson(route(self::ROTA, $this->produtoId), $produto);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria_id',
                ],
            ]);
    }

    public function testSucesso()
    {
        $produto = Produto::factory()->make();
        //dd($produto->getAttributes());
        $response = $this->putJson(route(self::ROTA, $this->produtoId), $produto->toArray());
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria',
                ],
            ]);
    }
}
