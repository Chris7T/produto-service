<?php

namespace Tests\Feature\Produto;

use App\Models\Produto\Produto;
use Tests\TestCase;

class CriarTest extends TestCase
{
    private const ROTA = 'produto.store';
    private const ID_INVALIDO = 0;

    public function testFalhaValoresGrandes()
    {
        $valoresGrandes = str_pad('', 101, 'A');
        $produtoDados   = Produto::factory()->make([
            'descricao' => $valoresGrandes,
            'nome'      => $valoresGrandes,
        ]);

        $response = $this->postJson(route(self::ROTA), $produtoDados->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'descricao',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $produtoDados = [
            'descricao' => null,
            'nome' => null,
            'preco_atual' => null,
            'categoria_id' => null,
        ];

        $response = $this->postJson(route(self::ROTA), $produtoDados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria_id',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $produto = [
            'descricao'    => 12,
            'nome'         => 12,
            'preco_atual'  => false,
            'categoria_id' => 12,
        ];

        $response = $this->postJson(route(self::ROTA), $produto);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria_id',
                ],
            ]);
    }

    public function testFalhaCategoriaInvalida()
    {
        $produto = Produto::factory()->make([
            'categoria_id' => self::ID_INVALIDO,
        ]);

        $response = $this->postJson(route(self::ROTA), $produto->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'categoria_id',
                ],
            ]);
    }

    public function testSucesso()
    {
        $produto = Produto::factory()->make();

        $response = $this->postJson(route(self::ROTA), $produto->toArray());
        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria',
                ],
            ]);
    }
}
