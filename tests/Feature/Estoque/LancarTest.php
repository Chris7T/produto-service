<?php

namespace Tests\Feature\Estoque;

use App\Models\Produto\Estoque;
use App\Models\Produto\Produto;
use Tests\TestCase;

class LancarTest extends TestCase
{
    private const ROTA = 'estoque.atualizar';
    private const ID_INVALIDO = 0;

    public function setUp(): void
    {
        parent::setUp();
        $this->produtoId = Produto::factory()->create()->getKey();
    }

    public function testFalhaDadosObrigatorios()
    {
        $dados = [
            'produto_id' => null,
            'quantidade' => null
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'produto_id',
                    'quantidade'
                ],
            ]);
    }

    public function testFalhaTipoDados()
    {
        $dados = [
            'produto_id' => "Dado Inválido",
            'quantidade' => "Dado Inválido"
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'produto_id',
                    'quantidade'
                ],
            ]);
    }

    public function testFalhaProdutoInvalido()
    {
        $dados = Estoque::factory()->make(['produto_id' => self::ID_INVALIDO])->toArray();

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'produto_id',
                ],
            ]);
    }

    public function testFalhaQuantidadeInvalido()
    {
        $dados = Estoque::factory()->make(['quantidade' => -1])->toArray();

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'quantidade',
                ],
            ]);
    }

    public function testSucesso()
    {
        $dados = Estoque::factory()->make(['produto_id' => $this->produtoId])->toArray();

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria',
                ],
            ]);
    }
}
