# Sistema de Controle de Produtos

Sistema oferece como serviço um controle de Produtos e também a realização do controle de estoque.

# Requisitos

- O sistema foi feito usando o Laravel 8 e o PHP 8 então é necessario a instalação dessas ferramentas para evitar imcompatibilidade. 

- O Banco de dados é necessário e deverá ser informado no `.env`.

## Documentação

Todo o sistema foi documentado usando a ferramenta `Scribe`.

Para a geração basta executar o comando abaixo.
```bash
php artisan scribe:generate
```

Para acessa-la depois da gera-la basta acessar.
```bash
http://{LINK}/docs
```

## Teste
Todas as funcionalidades podem ser testadas para verificar a integridade do sistema através do comando.
```bash
php artisan test
```

## Requisições
Para fazer a utilização da API é necessario configurar os cabeçalhos `Accept` e `Content-Type` com o valor `application/json`.