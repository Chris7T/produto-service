<?php

namespace RegrasNegocio;

use App\Http\Requests\Estoque\EstoqueRequest;
use App\Http\Resources\Produto\ProdutoResource as Resource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Produto\Produto;

class Estoque
{
    public function atualizarEstoque(EstoqueRequest $request): JsonResource
    {
        $produto = Produto::find($request->input('produto_id'));
        if ($produto) {
            $produto->estoque()->update(['quantidade' => $request->input('quantidade')]);
            return new Resource($produto);
        }
    }
}
